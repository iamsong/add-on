랜처 클러스터 템플릿 ADD-ON
---
# 개요
  # Rancher에서 클러스터 생성시 초기에 서비스, pod등을 배포 및 설정할 수 있다.
  
# 참조
  # https://rancher.com/docs/rke/latest/en/config-options/add-ons/user-defined-add-ons/#referencing-yaml-files-for-add-ons
```
addons_include:
    - https://raw.githubusercontent.com/rook/rook/master/cluster/examples/kubernetes/ceph/operator.yaml
    - https://raw.githubusercontent.com/rook/rook/master/cluster/examples/kubernetes/ceph/cluster.yaml
    - /opt/manifests/example.yaml
    - ./nginx.yaml
```    